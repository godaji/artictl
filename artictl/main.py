from __future__ import print_function
from __future__ import absolute_import


import fire

class ArtiCtl(object):
  """ArtiCtl"""

  def double(self, number):
    return 2 * number


if __name__ == '__main__':
  fire.Fire(ArtiCtl)
